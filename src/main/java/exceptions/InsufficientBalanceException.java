package exceptions;
//Abschnitt D
public class InsufficientBalanceException extends Exception {
    public InsufficientBalanceException() {
        super("Insufficient Account Balance");
    }
}
