package exceptions;
//Abschnitt I
public class GetCurrentPriceException extends Exception {
    public GetCurrentPriceException(String message){
        super(message);
    }
}
