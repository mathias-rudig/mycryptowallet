package exceptions;
//Abschnitt D
public class InsufficientAmountException extends Exception {
    public InsufficientAmountException() {
        super("Insuffiicient Amount of Crypto in Wallet");
    }
}
