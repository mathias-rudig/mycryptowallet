package exceptions;
//AbschnittJ
public class RetrieveDataException extends Exception {
    public RetrieveDataException(String message) {
        super(message);
    }
}
