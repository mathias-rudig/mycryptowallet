package domain;

import exceptions.RetrieveDataException;
import exceptions.SaveDataException;

/**
 * Das Interfaces DataStore, stellt Methoden zum Speichern und Auslesen von Objekten der
 * Klasse BankAccount und Walletlist bereit.
 *
 * @author mathiasrudig
 * @version 1.0
 */
//Abschnitt J
public interface DataStore {
    void saveBankAccount(BankAccount bankAccount) throws SaveDataException;

    void saveWalletlist(WalletList walletList) throws SaveDataException;

    BankAccount retrieveBankAccount() throws RetrieveDataException;

    WalletList retrieveWalletList() throws RetrieveDataException;
}
