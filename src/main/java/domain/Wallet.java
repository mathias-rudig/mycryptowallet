package domain;

import exceptions.InsufficientAmountException;
import exceptions.InsufficientBalanceException;
import exceptions.InvalidAmountException;
import exceptions.InvalidFeeException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * In dieser Klasse wird eine Wallet implementiert, in der Kryptowährunge respektive Transaktionslisten
 * gespeichert werden. In der Wallet können Kryptowährungen verkauft beziehungsweise verkauft werden.
 * Zudem enthält sie Gebühren welche per Businesslogik nur einen Wert größer gleich 0 sein dürfen.
 *
 * @author mathiasrudig
 * @version 1.0
 */
//Abschnitt E
public class Wallet implements Serializable {
    private final UUID id;
    private final String name;
    private final CryptoCurrency cryptoCurrency;
    private BigDecimal amount;
    private final List<Transaction> transactions;
    private BigDecimal feeInPercent;

    /**
     * Der Konstruktor erzeugt eine Wallet, welche einen Namen, eine bestimmte Kryptowährung und einen Definierten
     * Prozentsatz an Gebühren entgegen nimmt, welche der Methode setNewFee für die Überfrüfung übergeben wird.
     * Durch den Methodenaufruf muss beim Aufrufen des Konstruktors die gegebenfalls aufgerufene
     * Exception(InvalidFeeException) berücksichtigt werden(Bubbling).Die Restlichen Datenfelder werden per Default
     * gesetzt.
     *
     * @param name           übergebener Name der Wallet vom Typ String
     * @param cryptoCurrency übergebne Kryptowährung vom Typ CryptoCurrency
     * @param feeInPercent   übergebener Prozentsatz der Gebühren vom typ BibDecimal
     * @throws InvalidFeeException
     */
    //Abschnitt E
    public Wallet(String name, CryptoCurrency cryptoCurrency, BigDecimal feeInPercent) throws InvalidFeeException {
        this.id = UUID.randomUUID();
        this.name = name;
        this.cryptoCurrency = cryptoCurrency;
        this.amount = new BigDecimal("0");
        this.transactions = new ArrayList<>();
        this.setNewFee(feeInPercent);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public CryptoCurrency getCryptoCurrency() {
        return cryptoCurrency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Die Methode gibt eine Kopie der Transaktionsliste zurück, damit keine Manipulation von Außen möglich ist.
     *
     * @return gibt eine Liste von Transaktionen zurück
     */
    //Abschnitt E
    public List<Transaction> getTransactions() {
        return List.copyOf(transactions);
    }

    public BigDecimal getFeeInPercent() {
        return feeInPercent;
    }

    public String getCurrencyName() {
        return this.cryptoCurrency.getCurrencyName();
    }

    /**
     * Die Methode setzt den Prozentsatz der Gebühren auf den übergebenen Wert, welcher per if-Abfrage auf
     * einen Wert größer gleich null geprüft wird(Businessregel). Ansonsten wird eine Exception(InvalidFeeException)
     * geschmissen, um eine Fehlermeldung ausgeben zu können.
     *
     * @param fee übergebener Prozentsatz der Gebühren vom typ BibDecimal
     */
    //Abschnitt E
    public void setNewFee(BigDecimal fee) throws InvalidFeeException {
        if (fee.compareTo(new BigDecimal("0")) >= 0) {
            this.feeInPercent = fee;
        } else {
            throw new InvalidFeeException();
        }
    }

    /**
     * Die Methode implementiert das Kaufen von Kryptowährungen innerhalb einer erstellten Transaktion,
     * welche als Businessregel einen positiven Betrag größer 0 erwartet. Ansonsten wir eine
     * Exception(InvalidAmountException) geschmissen. Zudem wird die Transaktion in die Transaktionsliste
     * angefügt und der Betrag unter Berücksichtigung der Gebühren, dem BankAccount abgezogen.
     *
     * @param amount       übergebener Betrag vom Typ BigDecimal
     * @param currentPrice übergebener aktueller Preis vom Typ BigDecimal
     * @param bankAccount  übergebener Bankaccount vom Typ BankAccount
     * @throws InvalidAmountException
     * @throws InsufficientBalanceException
     */
    //Abschnitt F
    public void buy(BigDecimal amount, BigDecimal currentPrice, BankAccount bankAccount) throws InvalidAmountException, InsufficientBalanceException {
        if (amount.compareTo(new BigDecimal("0")) <= 0) {
            throw new InvalidAmountException();
        }
        Transaction transaction = new Transaction(this.cryptoCurrency, amount, currentPrice.setScale(6, RoundingMode.HALF_UP));
        //Abbuchen auf den BankAccount unter Berücksichtigung der Gebühren
        bankAccount.withdraw(transaction.getTotal().multiply(new BigDecimal("100").add(this.feeInPercent).divide(new BigDecimal("100").setScale(6, RoundingMode.HALF_UP))));
        this.transactions.add(transaction);
        this.amount = this.amount.add(transaction.getAmount());
    }

    /**
     * Die Methode implementiert das Verkaufen von Kryptowährungen innerhalb einer erstellten Transaktion,
     * welche als Businessregel einen positiven Betrag größer 0 erwartet. Ansonsten wir eine
     * Exception(InvalidAmountException) oder bei zu hohem Betrag die Exception(InsufficientAmountException)
     * geschmissen. Zudem wird die Transaktion in die Transaktionsliste angefügt und der Betrag unter
     * Berücksichtigung der Gebühren, dem BankAccount gutgeschrieben.
     *
     * @param amount       übergebener Betrag vom Typ BigDecimal
     * @param currentPrice übergebener aktueller Preis vom Typ BigDecimal
     * @param bankAccount  übergebener Bankaccount vom Typ BankAccount
     * @throws InsufficientAmountException
     * @throws InvalidAmountException
     */
    //Abschnitt G
    public void sell(BigDecimal amount, BigDecimal currentPrice, BankAccount bankAccount) throws InsufficientAmountException, InvalidAmountException {
        if (amount.compareTo(new BigDecimal("0")) <= 0) {
            throw new InvalidAmountException();
        }
        BigDecimal reducedAmount = this.amount.subtract(amount);
        if (reducedAmount.compareTo(new BigDecimal("0")) < 0) {
            throw new InsufficientAmountException();
        }
        //Transaktion mit einem negierten Betrag
        Transaction transaction = new Transaction(this.cryptoCurrency, amount.negate(), currentPrice.setScale(6, RoundingMode.HALF_UP));
        //Aufbuchen auf den BankAccount unter Berücksichtigung der Gebühren
        bankAccount.deposit(transaction.getTotal().negate().multiply(new BigDecimal("100").subtract(feeInPercent).divide(new BigDecimal("100"))));
        this.transactions.add(transaction);
        this.amount = reducedAmount;
    }


    /**
     * Die Methode gibt alle Datenfelder in Form eines Strings zurück.
     *
     * @return Datenfelder vom Typ String
     */
    //Abschnitt E
    @Override
    public String toString() {
        return "Wallet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cryptoCurrency=" + cryptoCurrency +
                ", amount=" + amount +
                ", transactions=" + transactions +
                ", feeInPercent=" + feeInPercent +
                '}';
    }
}
