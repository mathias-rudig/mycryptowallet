package domain;

import exceptions.GetCurrentPriceException;

import java.math.BigDecimal;

/**
 * Das Interface repräsentiert die Methodensignatur um den aktuellen Preis einer bestimmten Kryptowährung
 * ermiteln zu können und diesen Wert zurückzugeben.
 */
//Abschnitt I
public interface CurrentPriceForCurrency {

    BigDecimal getCurrentPrice(CryptoCurrency cryptoCurrency) throws GetCurrentPriceException;

}
