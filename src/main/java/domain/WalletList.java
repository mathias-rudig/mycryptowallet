package domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Die Klasse implementiert eine Liste von Wallets, welche von der Datenstruktur HashMap gealten werden und nur
 * einmal pro Kryptowährung vorkommen dürfen.
 *
 * @author mathiasrudig
 * @version 1.0
 */
//Abschnitt H
public class WalletList implements Serializable {
    private final HashMap<CryptoCurrency, Wallet> wallets;

    /**
     * Der Konstruktor erzeugt eine neue HashMap.
     */
    //Abschnitt H
    public WalletList() {
        this.wallets = new HashMap<>();
    }

    /**
     * Die Methode implementiert das hinzufügen von Wallets einer bestimmten Kryptowähren. Es darf nur eine Wallet
     * pro Kryptowährung geben(Businesslogik), was mit einer Bedingten Verzweigung umgesetzt wird.
     *
     * @param wallet ubergebene Wallet die hinzugefügt werden soll, vom Typ Wallet
     */
    //Abschnitt H
    public void addWallet(Wallet wallet) {
        if (wallet != null && !this.wallets.containsKey(wallet.getCryptoCurrency())) {
            wallets.put(wallet.getCryptoCurrency(), wallet);
        }
    }

    /**
     * Die Methode gibt eine bestimmte Wallet zurück.
     *
     * @param cryptoCurrency ubergebne Kryptowährung vom Typ CryptoCurrency die das Wallet über den Key zurückgibt.
     * @return gibt eine bestimmte Wallet vom Typ Wallet zurück
     */
    //Abschnitt H
    public Wallet getWallet(CryptoCurrency cryptoCurrency) {
        return this.wallets.get(cryptoCurrency);
    }

    /**
     * Die Methode erzeugt eine Liste der Wallets, welche sich die Values holt, genereirten aus diesen einen
     * Datenstrom und erzeugt damit eine Liste, welche zurückgegeben wird.
     *
     * @return gibt eine Liste von Wallets vom Typ Wallet zurück
     */
    //Abschnitt H
    public List<Wallet> getWallesAsObservabletList() {
        return wallets.values().stream().collect(Collectors.toList());
    }

    /**
     * Die Methode gibt alle Datenfelder in Form eines Strings zurück.
     *
     * @return Datenfelder vom Typ String
     */
    //Abschnitt H
    @Override
    public String toString() {
        return "WalletList{" +
                "wallets=" + wallets +
                '}';
    }
}
