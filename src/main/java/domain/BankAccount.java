package domain;

import exceptions.InsufficientBalanceException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Die Klasse implementiert das Verhalten eines BankAccounts, auf dem Geld eingezahlt respektive abgehoben werden kann.
 * Das Abheben ist maximal bis auf Kontosatand null erlaubt und wurde als Businesslogik implementiert. Als einziges
 * Datefeld steht der Aktuelle Betrag(balance) vom Typ BigDecimal, welcher per sondierender-Methode von Außen abgerufen
 * werden kann.
 *
 * @author mathiasrudig
 * @version 1.0
 */
//Abschnitt C
public class BankAccount implements Serializable {
    /**
     * Der Kontostand wird als BigDecimal(Klasse) implementiert, um Werte auf bestimmte
     * Nachkommastellen genau berechnen zu können.
     */
    private BigDecimal balance;

    /**
     * Der Konstruktor initialisiert den Bankaccount mit dem Wert 0.
     */
    //Abschnitt C
    public BankAccount() {
        this.balance = new BigDecimal("0").setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Die Methode implementiert das Aufbuchen eines Gewissen Betrages.
     *
     * @param amount übergebner Aufzubuchender Betrag vom Typ BigDecimal.
     */
    //Abschnitt C
    public void deposit(BigDecimal amount) {
        if (amount != null) {
            this.balance = this.balance.add(amount).setScale(2, RoundingMode.HALF_UP);
        }
    }

    /**
     * Die Methode implementiert das Abheben eines Gewissen Betrages, welcher nur so groß sein darf,
     * dass das Konto nicht ins Minus fällt, das über eine if-Abfrage sichergestellt wird. Zudem
     * wird im Fehlerfall eine Exception(InsufficientBalanceException) geworfen.
     *
     * @param amount übergebener Abzuhebender Betrag vom Typ BigDecimal.
     * @throws InsufficientBalanceException
     */
    //Abschnitt C
    public void withdraw(BigDecimal amount) throws InsufficientBalanceException {
        if (amount != null) {
            if (this.balance.subtract(amount).doubleValue() >= 0) {
                this.balance = this.balance.subtract(amount).setScale(2, RoundingMode.HALF_UP);
            } else {
                throw new InsufficientBalanceException();
            }
        }
    }

    /**
     * Die Methode gibt alle Datenfelder in Form eines Strings zurück.
     *
     * @return Datenfelder vom Typ String
     */
    //Abschnitt C
    @Override
    public String toString() {
        return "BankAccount{" +
                "balance=" + balance +
                '}';
    }
}
