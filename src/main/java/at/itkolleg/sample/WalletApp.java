package at.itkolleg.sample;

import exceptions.RetrieveDataException;
import exceptions.SaveDataException;
import domain.BankAccount;
import domain.DataStore;
import domain.WalletList;
import infrastruktur.CurrentCurrencyPrices;
import infrastruktur.FileDataStore;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import ui.GlobalContext;

import java.io.IOException;
import java.util.ResourceBundle;

public class WalletApp extends Application {

    //UI Parts //Abschnitt M
    private static Stage mainStage;
    public static final String GLOBAL_WALLET_LIST = "walletlist";
    public static final String GLOBAL_BANK_ACCOUNT = "bankaccount";
    public static final String GLOBAL_CURRENT_CURRENCY_PRICES = "currencyprices";
    public static final String GLOBAL_SELECTED_WALLET = "selectedWallet"; //um eine selektierte Wallet in der HashMap zwischenspeichern zu können

    /**
     * Die Methode implementiert das Wechseln zwischen den übergebenen Szenen.
     *
     * @param fxmlFile       übergebene fxml-File-Dateiname vom Typ String
     * @param resourceBundle übergebenes resource-Bundle com Typ String
     * @throws Exception wird bei einem Ladefehler der Szene geschmissen.
     */
    //Abschnitt M
    public static void switchScene(String fxmlFile, String resourceBundle) {
        try {
            Parent root = FXMLLoader.load(WalletApp.class.getResource(fxmlFile), ResourceBundle.getBundle(resourceBundle));
            Scene scene = new Scene(root);
            mainStage.setScene(scene);
            mainStage.show();
        } catch (Exception exception) { //um jede Exception abzufangen
            WalletApp.showErrorDialog("Could not load new scene!");
            exception.printStackTrace();
        }
    }

    /**
     * Die Methode implementiert Alerts, welche mittels einen übergebenen String(message) aufgerufen werden
     * und vom Benutzer weckgeklickt werden müssen. Die Methode kann von überall und zum Beispiel beim Auftreten
     * von exceptions und Ausgabe von Fehlertexten verwedet werden.
     *
     * @param message übergebene Message vom Typ String.
     */
    //Abschnitt M
    public static void showErrorDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("An Exception occurred: " + message);
        alert.showAndWait();
    }

    //File-Handling-Parts //Abschnitt N

    /**
     * Die Methode implementiert das Auslesen eines geschpeicherten Bankaccounts aus einer Binärdatei und liefert
     * diesen dann zurück.
     *
     * @return Liefert einen ausgelesenen bankaccount vom Typ BankAccount zurück.
     * @throws RetrieveDataException wird bei einem LeseFehler geschmissen.
     */
    //Abschnitt N
    private BankAccount loadBankAccountFromFile() throws RetrieveDataException {
        DataStore dataStore = new FileDataStore();
        BankAccount bankAccount = dataStore.retrieveBankAccount();
        System.out.println("BANKACCOUNT LOADED!");
        return bankAccount;
    }

    /**
     * Die Methode implementiert das Auslesen einer geschpeicherten Wallet-List aus einer Binärdatei und liefert
     * diese dann zurück.
     *
     * @return Liefert einen ausgelesene Wallet-List vom Typ WalletList zurück.
     * @throws RetrieveDataException wird bei einem LeseFehler geschmissen.
     */
    //Abschnitt N
    private WalletList loadWalletlistFromFile() throws RetrieveDataException {
        DataStore dataStore = new FileDataStore();
        WalletList walletList = dataStore.retrieveWalletList();
        System.out.println("WALLETLIST LOADED!");
        return walletList;
    }

    /**
     * Die Methode implementiert das Speichern eines Bankaccounts in eine Binärdatei.
     *
     * @throws SaveDataException wird bei einem Schreibfehler geschmissen.
     */
    //Abschnitt N
    private void storeBankAccountToFile(BankAccount bankAccount) throws SaveDataException {
        DataStore dataStore = new FileDataStore();
        dataStore.saveBankAccount(bankAccount);
        System.out.println("SAVED BANKACCOUNT!");
    }

    /**
     * Die Methode implementiert das Speichern einer Wallet-List in eine Binärdatei.
     *
     * @throws SaveDataException wird bei einem Schreibfehler geschmissen.
     */
    //Abschnitt N
    private void storeWalletListToFile(WalletList walletList) throws SaveDataException {
        DataStore dataStore = new FileDataStore();
        dataStore.saveWalletlist(walletList);
        System.out.println("SAVED WALLETLIST!");
    }

    /**
     * Die Methode(live cycle method) wird beim Start ausgeführt, lädt aus evtl. vorhandenen Dateien und setzt die übergebene Szene
     * bzw. switched auf die main.fxml Datei.
     *
     * @param stage übergebene Stage vom Typ Stage
     * @throws IOException
     */
    @Override
    //Abschnitt M
    public void start(Stage stage) throws IOException {
        mainStage = stage; //setzen der Stage beim Starten
        BankAccount bankAccount = new BankAccount();
        WalletList walletList = new WalletList();
        //Load from file //Abschnitt N
        try {
            bankAccount = loadBankAccountFromFile();
        } catch (RetrieveDataException retrieveDataException) {
            WalletApp.showErrorDialog("Error on loading BankAccount data. Using new empty account!");
            retrieveDataException.printStackTrace();
        }
        //Abschnitt N
        try {
            walletList = loadWalletlistFromFile();
        } catch (RetrieveDataException retrieveDataException) {
            WalletApp.showErrorDialog("Error on loading Wallet-List data. Using new empty WalletList!");
            retrieveDataException.printStackTrace();
        }

        //Fill GlobalContext //Abschnitt O
        GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_WALLET_LIST, walletList);
        GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_BANK_ACCOUNT, bankAccount);
        GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_CURRENT_CURRENCY_PRICES, new CurrentCurrencyPrices());

        //Abschnitt Z
        mainStage.setOnCloseRequest(event -> event.consume()); //verhindern dass der X-Button eine Funktion hat

        //Abschnitt M
        WalletApp.switchScene("main.fxml", "at.itkolleg.sample.main");
    }

    /**
     * Die Methode(live cycle method) implementiert das Stoppen der Applikation und übernimmt das Speichern der Datensätze.
     */
    @Override
    //Abschnitt O
    public void stop() {
        //sollte ggf. noch auf den richtigen Typ überprüft werden
        WalletList walletList = (WalletList) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_WALLET_LIST);
        BankAccount bankAccount = (BankAccount) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_BANK_ACCOUNT);
        //Write to file
        try {
            storeWalletListToFile(walletList);
            storeBankAccountToFile(bankAccount);
        } catch (SaveDataException saveDataException) {
            WalletApp.showErrorDialog("Could not store bankaccount and / or wallet details");
            saveDataException.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
