package ui;

import java.util.HashMap;

/**
 * Die Klasse dient zum Austausch und Zwischenspeichern von Daten, um von den verschiedenen Szenen und der
 * WalletApp-Klasse, Daten zu holen respektive abzulegen.
 *
 * @author mathiasrudig
 * @version 1.0
 */
//Abschnitt O
public class GlobalContext {
    //Abschnitt O
    private static GlobalContext globalContext;
    private HashMap<String, Object> states; //Object um alle Objekte speichern zu könen (muss dann später
    // auf den richtigen Typ gecastet werden.)

    /**
     * Der Konstruktor ist nur von der Klasse selber aufrufbar.
     */
    //Abschnitt O
    private GlobalContext() {
        states = new HashMap<>();
    }

    /**
     * Die Methode liefert ein Objekt vom Typ GlobalContext zurück, welches nur einmal instanziert werden kann.
     * Dies wird über eine Bedingte-Verzweigung überprüft.
     * @return  Liefert ein Objekt vom Typ GlobalContext zurück
     */
    //Abschnitt O
    public static GlobalContext getGlobalContext() {
        if (globalContext == null) {
            //hier wird garantiert, dass nur eine Instanz von GlobalContext gebildet wird.
            globalContext = new GlobalContext();
        }
        return globalContext;
    }

    /**
     * Die Methode liefert uns eine Datenelement aus der Hashmap auf Basis des übergebenen Keys.
     * @param key übergebener Key vom Typ String.
     * @return liefert das übereinstimmende Objekt zum übergebenen Key vom Typ Object zurück.
     */
    //Abschnitt O
    public Object getStateFor(String key){
        return states.get(key);
    }

    /**
     * Die Methode implementiert das Hinzufügen von Status mir einem übergebenen Objekt und einem Key.
     * @param key übergebener Key für den HashMap Eintrag vom Typ String.
     * @param state übergebener Key für den HashMap Eintrag vom Typ Object.
     */
    //Abschnitt O
    public void putStateFor(String key, Object state){
        states.put(key, state);
    }

    /**
     * Die Methode implementiert das Entfernen eines bestimmten Eintrages auf Basis des übergebenen Keys.
     * @param key übergebener Key vom Typ String.
     */
    //Abschnitt O
    public void removeStateFor(String key){
        states.remove(key);
    }

    /**
     * Die Methode implementiert das Löschen aller Einträge aus der HashMap.
     */
    //Abschnitt O
    public void emptyAllStates(){
        states.clear();
    }

}
